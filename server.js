const express = require('express');
const path = require('path');
const cors = require('cors');

const app = express();

const corsOptions = {
    credentials: true
}

app.use(express.static(__dirname + '/dist/mangaweb'));
app.use(cors(corsOptions));

app.get('/*', function(req,res) {
    
res.sendFile(path.join(__dirname+'/dist/mangaweb/index.html'));
});

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 8080);