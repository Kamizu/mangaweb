import { MangaInfoComponent } from './manga-info/manga-info.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

const ROUTES = [
  { path: '', component: HomeComponent },
  { path: 'mangainfo', component: MangaInfoComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(ROUTES) ],
  declarations: [],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
