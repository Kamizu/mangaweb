import { Component, OnInit } from '@angular/core';
import { MangaService } from '../manga.service';

@Component({
  selector: 'app-manga-info',
  templateUrl: './manga-info.component.html',
  styleUrls: ['./manga-info.component.css']
})
export class MangaInfoComponent implements OnInit {
  public manga = {};
  public imageUrlPrefix;

  constructor(private _mangaService: MangaService) { }

  ngOnInit() {
    this.manga = this._mangaService.getSelectedManga();
    this.imageUrlPrefix = this._mangaService.getImageUrlPrefix();
  }
}
