import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class MangaService {
  constructor(private http: HttpClient) {}
  private _mangas = [];
  private _url = 'https://www.mangaeden.com/api/';
  private selectedManga;

  getMangaList() {
    return this.http.get(this._url + 'list/1/');
  }

  getManga(id) {
    return this.http.get(this._url + 'manga/' + id + '/');
  }

  getChapter(id) {
    return this.http.get(this._url + 'chapter/' + id + '/');
  }

  formatString(data) {
    if (!data) {
      return;
    }
    data = data.replace(/-/g, ' ');
    data = data.replace(/\w\S*/g, function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
    return data;
  }

  getImageUrlPrefix() {
    const IMAGE_URL_PREFIX = 'https://cdn.mangaeden.com/mangasimg/';
    return IMAGE_URL_PREFIX;
  }

  setSelectedManga(selected) {
    this.selectedManga = selected;
  }

  getSelectedManga() {
    if (this.selectedManga) {
      return this.selectedManga;
    }
  }
}
