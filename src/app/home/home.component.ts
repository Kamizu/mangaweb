import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MangaService } from '../manga.service';

@Component({
  selector: 'app-manga-list',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public mangas = [];
  public _imageUrlPrefix;
  public itemsToLoad = 36;
  public itemsToShowInitially = 24;
  public itemsToShow;
  constructor(private _mangaService: MangaService) { }

  ngOnInit(): any {
    this._mangaService.getMangaList()
        .subscribe(
          data => {
            this.mangas = data['manga'];
            this.mangas.forEach(res => {
              res.a = this._mangaService.formatString(res.a);

              if (res.im) {
                res.im = res.im.replace(/ /g, '');
              }
            });
          },
          (err) => console.error(err),
          () => {
            this.itemsToShow = this.mangas.slice(0, this.itemsToShowInitially);
          });
    this._imageUrlPrefix = this._mangaService.getImageUrlPrefix();
  }

  onScroll() {
    if (this.itemsToShowInitially <= this.mangas.length) {
      this.itemsToShowInitially += this.itemsToLoad;
      this.itemsToShow = this.mangas.slice(0, this.itemsToShowInitially);
      console.log('scrolled');
    }
  }

  selectManga(selected) {
    this._mangaService.setSelectedManga(selected);
  }
}
